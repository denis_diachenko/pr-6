import React from 'react';

const delay = (t) => new Promise((r) => setTimeout(r, t));

class App extends React.Component {
  state = {
    todos: [],
    newTodoTitle: '',
    isTodosLoading: true,
  };

  async componentDidMount() {
    await this.loadTodos();
  }

  loadTodos = async () => {
    this.setState({ isTodosLoading: true });
    await delay(1500);
    const res = await fetch(
      'https://jsonplaceholder.typicode.com/todos?_limit=5'
    );
    const todos = await res.json();
    this.setState({
      isTodosLoading: false,
      todos: todos.sort((a, b) => {
        // the right way to sort strings :)
        return a.title.localeCompare(b.title, 'en', { sensitivity: 'base' });
      }),
    });
  };

  addTodo = async () => {
    if (this.state.newTodoTitle.trim()) {
      return;
    }
    const newTodo = {
      title: this.state.newTodoTitle,
      completed: false,
    };
    const res = await fetch('https://jsonplaceholder.typicode.com/todos/', {
      method: 'todo',
      body: JSON.stringify(newTodo),
    });
    const id = await res.json();

    this.setState((prev) => {
      return {
        newTodoTitle: '',
        todos: [{ ...newTodo, id }, ...prev.todos],
      };
    });
  };

  deleteTodo = async (id) => {
    await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      method: 'delete',
    });
    this.setState((prev) => ({
      todos: prev.todos.filter((item) => item.id !== id),
    }));
  };

  completeTodo = async (id) => {
    await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      method: 'put',
      body: JSON.stringify({ completed: true }),
    });
    this.setState((prev) => {
      prev.todos.find((item) => item.id === id).completed = true;
      return {
        todos: [...prev.todos],
      };
    });
  };

  render() {
    const { todos, newTodoTitle, isTodosLoading } = this.state;
    return (
      <div className="container">
        <h1>
          React <sub>js fetch</sub>
        </h1>

        <div className="card">
          <div className="form-control">
            <input
              type="text"
              placeholder="Название"
              value={newTodoTitle}
              onChange={(ev) =>
                this.setState({ newTodoTitle: ev.target.value })
              }
            />
          </div>
          <button className="btn" onClick={this.addTodo}>
            Добавить
          </button>
          <button className="btn" onClick={this.loadTodos}>
            Загрузить
          </button>
        </div>

        <hr />
        {isTodosLoading ? (
          <div>Loading:::</div>
        ) : (
          todos.map((todo) => (
            <div className="card" key={todo.id}>
              <p>
                <span className={todo.completed ? 'completed' : ''}>
                  {todo.title}
                </span>
                <span>
                  <button
                    className="btn btn-danger"
                    onClick={() => this.deleteTodo(todo.id)}
                  >
                    Удалить
                  </button>
                  <button
                    className="btn"
                    disabled={todo.completed}
                    onClick={() => this.completeTodo(todo.id)}
                  >
                    Завершить
                  </button>
                </span>
              </p>
            </div>
          ))
        )}
      </div>
    );
  }
}

export default App;
